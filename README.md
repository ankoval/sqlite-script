# SQLite script

This is a set of scripts to simplify creation of the SQLite database. There are a few tricky things you need to know.

## Provisioning env

Before starting the work please modify `./sqlite_make.sh` to source all necessary environments. The main requirement is TGeant built with `efficienciesDB`.

## Usage

Usage of this script can be obtained as `./sqlite_make.sh`:

```
usage: sqlite_make.py [-h] --dbpath DBPATH --year YEAR [--detectors DETECTORS]

Assembles the SQLite database of given histogram files

optional arguments:
  -h, --help            show this help message and exit
  --dbpath DBPATH       Output database path
  --year YEAR           Year to which register histograms
  --detectors DETECTORS
                        Used to initialize a new year
```

Say you have your PHAST output merged into a singular roots per tbname and now you want to make SQLite DB out of it. So you listed all of these files into `merged_ue11_output.txt`, you typically will run something like this:

```
./sqlite_make.sh \
    --dbpath ./dvcs.2016.p07.efficiencies.sqlite \
    --year 2016_P07_MINUS \
    --detectors <path to>/detectors.dat < merged_ue11_output.txt
```

But if you first want initialize database with `detectors.dat` corresponding to your period of data follow the instructions of the next section.

### The magic number for init

To also properlly initialize database with `detectors.dat`, you have to place `42` at the beginning of the list of your files. So the list of your PHAST output would look like:

```
42
/home1/06955/akoval/scratch3/dvcs-P09.mu.plus.good_cuts.ue11.29.09.2020/run_hadd_aa_1/output/DC01U_0.root
/home1/06955/akoval/scratch3/dvcs-P09.mu.plus.good_cuts.ue11.29.09.2020/run_hadd_aa_1/output/DC04Y_0.root
/home1/06955/akoval/scratch3/dvcs-P09.mu.plus.good_cuts.ue11.29.09.2020/run_hadd_aa_1/output/DC05Y_0.root
/home1/06955/akoval/scratch3/dvcs-P09.mu.plus.good_cuts.ue11.29.09.2020/run_hadd_aa_1/output/DW01X_0.root
...
```

This `42` is required for `efficienciesDB` as a "magic input" to make sure you know what you are doing.

## Outputs

The main output will be within the sqlite file you set as an argument, but besides of this script will create following folders: `RT`, `err`, `png`, `map`.

Be aware that sscript will produce lots of output into stdout. Overall runtime will take about 1-2 hours.

## Additional reading

https://wwwcompass.cern.ch/compass/software/offline/TGeant/TGeantOldPage/na58-project-tgeant.web.cern.ch/content/2-dimensional-efficiency-maps.html

## Disclamer

This script was used a few times and might exert strange behaviour. Let me know if something like this would happen. Also I would be very much interested in any cornercases you could provide or just any suggestions.
