#!/usr/bin/env bash

current_dir="${PWD}"

# NOTE: source all required environments right after this line

cd "${current_dir}"

./sqlite_make.py ${@}
