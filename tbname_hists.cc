double histMean(TH1* rt, bool allow_all = false) {
    if (!rt) return 0;
    double SumUp = 0.;
    unsigned int nbins = 0U;
    for(int xi = 1; xi <= rt->GetNbinsX(); xi++){
        if (allow_all || rt->GetBinContent(xi) > 0) {
            SumUp += rt->GetBinContent(xi);
            nbins += (rt->GetBinContent(xi) != 0);
        }
    }
    return SumUp / nbins;
}

double histMeanError(TH1* rt, bool allow_all = false) {
    if (!rt) return 0;
    double SumUp = 0.;
    unsigned int nbins = 0U;
    for(int xi = 1; xi <= rt->GetNbinsX(); xi++){
        if (allow_all || rt->GetBinContent(xi) > 0) {
            SumUp += rt->GetBinError(xi);
            nbins += (rt->GetBinContent(xi) != 0);
        }
    }
    return SumUp / nbins;
}

double histMean(TH2* rt, bool allow_all = false) {
    if (!rt) return 0;
    double SumUp = 0.;
    unsigned int nbins = 0U;
    for(int xi = 1; xi <= rt->GetNbinsX(); xi++){
        for(int yi = 1; yi <= rt->GetNbinsY(); yi++){
            if (allow_all, rt->GetBinContent(xi, yi) > 0) {
                SumUp += rt->GetBinContent(xi, yi);
                nbins += (rt->GetBinContent(xi, yi) != 0);
            }
        }
    }
    return SumUp / nbins;
}

double histMeanError(TH2* rt, bool allow_all = false) {
    if (!rt) return 0;
    double SumUp = 0.;
    unsigned int nbins = 0U;
    for(int xi = 1; xi <= rt->GetNbinsX(); xi++){
        for(int yi = 1; yi <= rt->GetNbinsY(); yi++){
            if (allow_all || rt->GetBinContent(xi, yi) > 0) {
                SumUp += rt->GetBinError(xi, yi);
                nbins += (rt->GetBinContent(xi, yi) != 0);
            }
        }
    }
    return SumUp / nbins;
}

double computeMeanYann(TH2D* num, TH2D* den){
    bool useMe(false);
    TH2D* rt = (TH2D*) num->Clone();
    rt->Divide(den);
    return histMean(rt);
}

TH2D* ComputeEMaps(TH2D* num, TH2D* den){
    bool useMe(false);
    cout << num->GetEntries() << " -- " << den->GetEntries() << endl;
    TH2D* rt = (TH2D*)num->Clone();
    rt->Divide(den);
    TH2D* res = (TH2D*)rt->Clone();

    double err = 0;
    double SumUp = 0;
    for(int xi = 1; xi <= num->GetNbinsX(); xi++){
        for(int yi = 1; yi <= num->GetNbinsY(); yi++){
            res->SetBinContent(xi,yi,1);
            err = (1/den->GetBinContent(xi,yi))*sqrt(rt->GetBinContent(xi,yi)*(1-(rt->GetBinContent(xi,yi))/den->GetBinContent(xi,yi)));
            if(den->GetBinContent(xi,yi) != 0){
                res->SetBinContent(xi,yi,err);
            }
            if(den->GetBinContent(xi,yi) == 0 || num->GetBinContent(xi,yi) == 0 || (den->GetBinContent(xi,yi) == 1 && rt->GetBinContent(xi,yi) == 1)){
                res->SetBinContent(xi,yi,1);
            }
            if(err != err){
                res->SetBinContent(xi,yi,1);
            }
        }
    }
    return res;
}

class EfficiencyHistograms {
  static std::string const eff_dir;
  static std::string const Nsigma;

  std::string const source_file_name;
  std::string const detector;
  TFile * source_file;
  TH2D * _he2; bool _is_computed_he2;
  TH1D * _he1; bool _is_computed_he1;
  TH1D * _hn; bool _is_computed_hn;
  TH2D * _hc2; bool _is_computed_hc2;
  TH1D * _hc1; bool _is_computed_hc1;
  TH2D * _ratio2; bool _is_computed_ratio2;
  TH1D * _ratio1; bool _is_computed_ratio1;

public:
  EfficiencyHistograms(std::string const & filepath, std::string const & tbname)
   : source_file_name(filepath), detector(tbname), source_file(TFile::Open(filepath.c_str())),
     _he2(nullptr), _is_computed_he2(false),
     _he1(nullptr), _is_computed_he1(false),
     _hn(nullptr), _is_computed_hn(false),
     _hc2(nullptr), _is_computed_hc2(false),
     _hc1(nullptr), _is_computed_hc1(false),
     _ratio2(nullptr), _is_computed_ratio2(false),
     _ratio1(nullptr), _is_computed_ratio1(false){}

  ~EfficiencyHistograms() { source_file->Close(); }

  TH1D * get_hist(std::string const & name) {
    std::string hist_name(eff_dir + name + detector);
    return (TH1D *) source_file->Get(hist_name.c_str());
  }

  TH2D * get_he2d() {
    if (_he2 || _is_computed_he2) return _he2;
    _is_computed_he2 = true;
    _he2 = (TH2D *) get_hist("he2");

    if (!_he2) return _he2;

    TH2D * expected_hits = (TH2D *) get_hist("hE2");
    TH2D * found_hits = (TH2D *) get_hist("hF2");

    _he2->Sumw2();
    _he2->Divide(found_hits, expected_hits, 1, 1, "B");

    std::ostringstream meanY;
    meanY << histMean(_he2) * 100.;
    _he2->SetTitle((detector + ": Efficiency ("+ Nsigma +" #sigma) = " + meanY.str() + " %").c_str());

    char header[] = "TB0iCjxy: Efficiency #times Acceptance - Background (10#sigma)         ";
    std::string header_format(detector + ": Efficiency (#sigma) = %.2f#pm%.2f%%");
    sprintf(header, header_format.c_str(), 100 * histMean(_he2), 100 * histMeanError(_he2));
    _he2->SetTitle(header);

    _he2->SetEntries(expected_hits->GetEntries());

    return _he2;
  }

  TH1D * get_he1d() {
    if (_he1 || _is_computed_he1) return _he1;
    _is_computed_he1 = true;

    _he1 = get_hist("he1");
    if (!_he1) return _he1;

    TH1D * expected_hits = (TH1D *) get_hist("hE1");
    TH1D * found_hits = (TH1D *) get_hist("hF1");

    _he1->Divide(found_hits, expected_hits, 1, 1, "B");

    std::ostringstream meanY;
    meanY << histMean(_he2) * 100.;
    _he1->SetTitle((detector + ": Efficiency ("+ Nsigma +" #sigma) = " + meanY.str() + " %").c_str());

    char header[] = "TB0iCjxy: Efficiency #times Acceptance - Background (10#sigma)         ";
    std::string header_format(detector + ": Efficiency (#sigma) = %.2f#pm%.2f%%");
    sprintf(header, header_format.c_str(), 100 * histMean(_he1), 100 * histMeanError(_he1));
    _he1->SetTitle(header);

    _he1->SetEntries(expected_hits->GetEntries());

    return _he1;
  }

  TH1D * get_hn() {
    if (_hn || _is_computed_hn) return _hn;
    _is_computed_hn = true;
    _hn = get_hist("hn1");

    if (!_hn) return _hn;

    TH1D * background_signal = get_hist("hB1");
    TH1D * random_signal = get_hist("hR1");

    _hn->Sumw2();
    _hn->Divide(background_signal, random_signal, 1, 1, "B");
    _hn->SetEntries(random_signal->GetEntries());

    return _hn;
  }

  TH2D * get_hc2d() {
    if (_hc2 || _is_computed_hc2) return _hc2;
    _is_computed_hc2 = true;

    TH1 * he = (TH1 *) get_he2d();

    if (!he) return nullptr;

    TH1 * hE = get_hist("hE2");
    TH1 * hF = get_hist("hF2");
    _hc2 = (TH2D *) get_hist("hc2");

    if (!_hc2) return nullptr;

    double N = hF->GetEntries();
    double D = hE->GetEntries();
    double e = N / D;
    double de = sqrt(e * (1 - e) / D);

    TH1 * hn = get_hn();
    TH1 * hR = get_hist("hR1");
    TH1 * hB = get_hist("hB1");

    double B = hB->GetEntries();
    double R = hR->GetEntries();
    double n = B / R;
    double dn = sqrt(n * (1 - n) / R);

    for (int binx = 1; binx <= _hc2->GetNbinsX(); ++binx) {
      double n = hn->GetBinContent(binx);
      double dn = hn->GetBinError(binx);
      for (int biny = 1; biny <= _hc2->GetNbinsY(); ++biny) {
        double e =  he->GetBinContent(binx, biny);
        double de = he->GetBinError(binx, biny);
        double ec, dec;
        if (1 - n > 0) {
          ec = (e - n) / (1 - n);
          dec = sqrt(de * de * (1 - n) * (1 - n) + dn * dn * (1-e) * (1-e)) / (1 - n) / (1 - n);
        } else {
          ec = .5;
          dec = .5;
        }
        _hc2->SetBinContent(binx, biny, ec > 0 ? ec : 0);
        _hc2->SetBinError(binx, biny, dec);
      }
    }

    char header[] = "TB0iCjxy: Efficiency-Background #times Acceptance - Background (10#sigma)         ";
    sprintf(header, "${tbname}: Efficiency-Background (#sigma) = %.2f#pm%.2f%%", 100 * histMean(_hc2), 100 * histMeanError(_hc2));
    _hc2->SetTitle(header);
    printf("\n\t>>> ${tbname} - %.2f\n\n", 100 * histMean(_hc2));
    return (TH2D*) _hc2;
  }

  TH1D * get_hc1d() {
    if (_hc1 || _is_computed_hc1) return _hc1;
    _is_computed_hc1 = true;

    TH1 * he = (TH1 *) get_he1d();

    if (!he) return nullptr;

    TH1 * hE = get_hist("hE1");
    TH1 * hF = get_hist("hF1");
    _hc1 = get_hist("hc1");

    if (!_hc1) return nullptr;

    double N = hF->GetEntries();
    double D = hE->GetEntries();
    double e = N / D;
    double de = sqrt(e * (1 - e) / D);

    TH1 * hn = get_hn();
    TH1 * hR = get_hist("hR1");
    TH1 * hB = get_hist("hB1");

    double B = hB->GetEntries();
    double R = hR->GetEntries();
    double n = B / R;
    double dn = sqrt(n * (1 - n) / R);

    for (int binx = 1; binx <= _hc1->GetNbinsX(); ++binx) {
      double n = hn->GetBinContent(binx);
      double dn = hn->GetBinError(binx);
      double e =  he->GetBinContent(binx);
      double de = he->GetBinError(binx);
      double ec, dec;
      if (1 - n > 0) {
        ec = (e - n) / (1 - n);
        dec = sqrt(de * de * (1 - n) * (1 - n) + dn * dn * (1-e) * (1-e)) / (1 - n) / (1 - n);
      } else {
        ec = .5;
        dec = .5;
      }
      _hc1->SetBinContent(binx, ec > 0 ? ec : 0);
      _hc1->SetBinError(binx, dec);
    }

    char header[] = "TB0iCjxy: Efficiency-Background #times Acceptance - Background (10#sigma)         ";
    sprintf(header, "${tbname}: Efficiency-Background (#sigma) = %.2f#pm%.2f%%", 100 * histMean(_hc1), 100 * histMeanError(_hc1));
    _hc1->SetTitle(header);
    return (TH1D*) _hc1;
  }

  TH2D * get_ec_2dratio() {
    if (_ratio2 || _is_computed_ratio2) return _ratio2;
    _is_computed_ratio2 = true;

    if  (!get_hc2d()) return _ratio2;

    _ratio2 = (TH2D *) get_hc2d()->Clone();
    _ratio2->Divide(get_he2d());

    char header[] = "TB0iCjxy: Efficiency / Efficiency-Background #times Acceptance - Background (10#sigma)         ";
    sprintf(header, "${tbname}: Efficiency-Background / Efficiency (#sigma) = %.2f#pm%.2f%%", 100 * histMean(_ratio2), 100 * histMeanError(_ratio2));
    _ratio2->SetTitle(header);
    printf("\n\t>>2d> ${tbname} - %.2f\n\n", 100 * histMean(_ratio2));

    return _ratio2;
  }

  TH1D * get_ec_1dratio() {
    if (_ratio1 || _is_computed_ratio1) return _ratio1;
    _is_computed_ratio1 = true;

    if  (!get_hc1d()) return _ratio1;

    _ratio1 = (TH1D *) get_hc1d()->Clone();
    _ratio1->Divide(get_he1d());

    char header[] = "TB0iCjxy: Efficiency / Efficiency-Background #times Acceptance - Background (10#sigma)         ";
    sprintf(header, "${tbname}: Efficiency-Background / Efficiency (#sigma) = %.2f#pm%.2f%%", 100 * histMean(_ratio1), 100 * histMeanError(_ratio1));
    _ratio1->SetTitle(header);
    printf("\n\t>>1d> ${tbname} - %.2f\n\n", 100 * histMean(_ratio1));

    return _ratio1;
  }
};

class Drawer {
  static TCanvas * canvas;

public:
  static TCanvas * getCanvas() {
    if (canvas) return canvas;

    canvas = new TCanvas();
    canvas->SetCanvasSize(1920 * 1.5, 1080 * 1.5);
    gStyle->SetPalette(55);
    gStyle->SetOptStat(0);

    return canvas;
  }

  static void draw(TH1D * histogram, std::string const & filename) {
    if (!histogram) return;

    histogram->Draw("COLZ");
    getCanvas()->Print(filename.c_str());
    getCanvas()->Clear();
    getCanvas()->cd();
  }
  static void draw(TH2D * histogram, std::string const & filename) { draw((TH1D *) histogram, filename); }
};

TCanvas * Drawer::canvas = nullptr;

std::string const EfficiencyHistograms::eff_dir = "Efficiencies/";
std::string const EfficiencyHistograms::Nsigma = "6";

TH2D* remaster_eff_m_back(TFile * f, double norm) {
    TH1 *he = (TH1*)f->Get("Efficiencies/he2${tbname}");
    TH1 *hE = (TH1*)f->Get("Efficiencies/hE2${tbname}");
    TH1 *hF = (TH1*)f->Get("Efficiencies/hF2${tbname}");
    printf("P1\n");
    if (!he || !hE || !hF) { return NULL; }

//    he->Scale(1. / norm);
//    hE->Scale(1. / norm);
//    hF->Scale(1. / norm);
    TH1 *hc = (TH1*)f->Get("Efficiencies/hc2${tbname}");
    printf("P2\n");
    if (!hc) { return NULL; }

    hc->Scale(1. / norm);
    double N = hF->GetEntries();
    double D = hE->GetEntries();
    double e = N / D;
    double de = sqrt(e * (1 - e) / D);

    he->Sumw2();
    he->Divide(hF, hE, 1, 1, "B");
    he->SetEntries(D);

    TH1 *hn = (TH1*)f->Get("Efficiencies/hn1${tbname}");
    TH1 *hR = (TH1*)f->Get("Efficiencies/hR1${tbname}");
    TH1 *hB = (TH1*)f->Get("Efficiencies/hB1${tbname}");
    printf("P3\n");
    if (!hn || !hR || !hB) { return NULL; }

    hn->Scale(1. / norm);
    hR->Scale(1. / norm);
    hB->Scale(1. / norm);
    double B = hB->GetEntries();
    double R = hR->GetEntries();
    double n = B / R;
    double dn = sqrt(n * (1 - n) / R);

    hn->Sumw2();
    hn->Divide(hB, hR, 1, 1, "B");
    hn->SetEntries(R);

    hc->Sumw2();

    for (int binx = 1; binx <= hc->GetNbinsX(); binx++) {
      double n = hn->GetBinContent(binx);
      double dn = hn->GetBinError(binx);
      for (int biny = 1; biny <= hc->GetNbinsY(); biny++) {
        double e =  he->GetBinContent(binx, biny);
        double de = he->GetBinError(binx, biny);
        double ec, dec;
        if (1 - n > 0) {
          ec = (e - n) / (1 - n);
          dec = sqrt(de * de * (1 - n) * (1 - n) + dn * dn * (1-e) * (1-e)) / (1 - n) / (1 - n);
        } else {
          ec = .5;
          dec = .5;
        }
        hc->SetBinContent(binx, biny, ec);
        hc->SetBinError(binx, biny, dec);
      }
    }

    char hT[] = "TB0iCjxy: Efficiency-Background #times Acceptance - Background (10#sigma)         ";
    sprintf(hT, "${tbname}: Efficiency-Background (#sigma) = %.2f#pm%.2f%%", 100 * histMean(hc), 100 * histMeanError(hc));
    hc->SetTitle(hT);

    return (TH2D*)hc;
}

void decompose(TH2D * origin, TCanvas * c, std::string const & name, std::string const & title) {

  char slide_title[] = "${tbname} TB0iCjxy: [bins: 10000000] (-#infty .. #infty) Efficiency-Background #times Acceptance - Background / Efficiency-Background #times Acceptance - Background (10#sigma)         ";
  c->Divide(2, 2);

  TH2D * below = (TH2D *) origin->Clone();
  unsigned int below_bins = 0U;
  float all_bins = 0.;
  for(int xi = 1; xi <= below->GetNbinsX(); xi++){
    for(int yi = 1; yi <= below->GetNbinsY(); yi++){
      double value = below->GetBinContent(xi, yi);

      if (value >= 0) {
        below->SetBinContent(xi, yi, 0);
      } else {
        ++below_bins;
      }
      ++all_bins;
    }
  }
  all_bins /= 100.;
  below->SetMaximum(0);
  sprintf(slide_title, (std::string("${tbname}: [bins: %.2f%%] (-#infty .. 0)") + title + " = %.2f#pm%.2f%%").c_str(), below_bins / all_bins, 100 * histMean(below, true), 100 * histMeanError(below, true));
  below->SetTitle(slide_title);

  c->cd(1);
  below->Draw("COLZ");

  TH2D * lowwer = (TH2D *) origin->Clone();
  unsigned int lowwer_bins = 0U;
  for(int xi = 1; xi <= lowwer->GetNbinsX(); xi++){
    for(int yi = 1; yi <= lowwer->GetNbinsY(); yi++){
      double value = lowwer->GetBinContent(xi, yi);

      if (value > 0.75 || value < 0) {
        lowwer->SetBinContent(xi, yi, 0);
      } else {
        ++lowwer_bins;
      }
    }
  }

  lowwer->SetMaximum(0.75);
  lowwer->SetMinimum(0);
  sprintf(slide_title, (std::string("${tbname}: [bins: %.2f%%] (0 .. 0.75]") + title + " = %.2f#pm%.2f%%").c_str(), lowwer_bins / all_bins, 100 * histMean(lowwer, true), 100 * histMeanError(lowwer, true));
  lowwer->SetTitle(slide_title);

  c->cd(2);
  lowwer->Draw("COLZ");

  TH2D * midd = (TH2D *) origin->Clone();
  unsigned int midd_bins = 0U;
  for(int xi = 1; xi <= midd->GetNbinsX(); xi++){
    for(int yi = 1; yi <= midd->GetNbinsY(); yi++){
      double value = midd->GetBinContent(xi, yi);

      if (value <= 0.75 || value > 1) {
        midd->SetBinContent(xi, yi, 0);
      } else {
        ++midd_bins;
      }
    }
  }

  midd->SetMaximum(1);
  midd->SetMinimum(0.75);
  sprintf(slide_title, (std::string("${tbname}: [bins: %.2f%%] (0.75 .. 1]") + title + " = %.2f#pm%.2f%%").c_str(), midd_bins / all_bins, 100 * histMean(midd, true), 100 * histMeanError(midd, true));
  midd->SetTitle(slide_title);

  c->cd(3);
  midd->Draw("COLZ");


  TH2D * upp = (TH2D *) origin->Clone();
  unsigned int upper_bins = 0U;
  for(int xi = 1; xi <= upp->GetNbinsX(); xi++){
    for(int yi = 1; yi <= upp->GetNbinsY(); yi++){
      double value = upp->GetBinContent(xi, yi);

      if (value <= 1) {
        upp->SetBinContent(xi, yi, 0);
      } else {
        ++upper_bins;
      }
    }
  }
  upp->SetMinimum(1);
  sprintf(slide_title, (std::string("${tbname}: [bins: %.2f%%] (0.75 .. 1]") + title + " = %.2f#pm%.2f%%").c_str(), upper_bins / all_bins, 100 * histMean(upp, true), 100 * histMeanError(upp, true));
  upp->SetTitle(slide_title);

  c->cd(4);
  upp->Draw("COLZ");

  c->Print((std::string("png/") + name + "__sliced.png").c_str());
  c->Clear();
  c->cd();
}

void make_histograms_${tbname}() {
    string TBName = "${tbname}";

    EfficiencyHistograms cuts("${collective_root}", TBName);
    TFile* out = new TFile("maps/${tbname}.root", "RECREATE");
    out->mkdir("Efficiencies/");
    out->cd("Efficiencies/");
    cuts.get_he2d()->Write();
    cuts.get_hc2d()->Write();
    cuts.get_ec_2dratio()->Write();
    TH2D * expected_hits = (TH2D *) cuts.get_hist("hE2");
    expected_hits->Write();
    out->Close();
}

