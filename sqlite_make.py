#!/usr/bin/env python3

from subprocess import Popen
from argparse import ArgumentParser
from sys import stdin, stderr
from os import makedirs
from os.path import expandvars, abspath, normpath, basename, join, exists
from logging import DEBUG, INFO, Formatter, StreamHandler, getLogger
from typing import List, Tuple
from mako.template import Template

logger = getLogger(__file__)
logger.setLevel(DEBUG)

ch = StreamHandler(stream=stderr)
ch.setLevel(DEBUG)
ch.setFormatter(Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(ch)

DB_CFG = [
    ('BM01P1__', ('BMS1', 1)), ('BM02P1__', ('BMS2', 2)), ('BM03P1__', ('BMS3', 3)), ('BM04P1__', ('BMS4', 4)),
    ('BM05P1__', ('BMS5', 1)), ('BM06P1__', ('BMS6', 1)), ('DC00U1__', ('SDB5', 1)), ('DC00U2__', ('SDB6', 1)),
    ('DC00V1__', ('SDB7', 1)), ('DC00V2__', ('SDB8', 1)), ('DC00X1__', ('SDB3', 1)), ('DC00X2__', ('SDB4', 1)),
    ('DC00Y1__', ('SDB1', 1)), ('DC00Y2__', ('SDB2', 1)), ('DC01U1__', ('SDB5', 2)), ('DC01U2__', ('SDB6', 2)),
    ('DC01V1__', ('SDB7', 2)), ('DC01V2__', ('SDB8', 2)), ('DC01X1__', ('SDB3', 2)), ('DC01X2__', ('SDB4', 2)),
    ('DC01Y1__', ('SDB1', 2)), ('DC01Y2__', ('SDB2', 2)), ('DC04U1__', ('SDC2', 1)), ('DC04U2__', ('SDC1', 1)),
    ('DC04V1__', ('SDC4', 1)), ('DC04V2__', ('SDC3', 1)), ('DC04X1__', ('SDC6', 1)), ('DC04X2__', ('SDC5', 1)),
    ('DC04Y1__', ('SDC8', 1)), ('DC04Y2__', ('SDC7', 1)), ('DC05U1__', ('SDC2', 2)), ('DC05U2__', ('SDC1', 2)),
    ('DC05V1__', ('SDC4', 2)), ('DC05V2__', ('SDC3', 2)), ('DC05X1__', ('SDC6', 2)), ('DC05X2__', ('SDC5', 2)),
    ('DC05Y1__', ('SDC8', 2)), ('DC05Y2__', ('SDC7', 2)), ('DW01X1__', ('DW11', 1)), ('DW01X2__', ('DW12', 1)),
    ('DW01Y1__', ('DW13', 1)), ('DW01Y2__', ('DW14', 1)), ('DW02X1__', ('DW21', 1)), ('DW02X2__', ('DW22', 1)),
    ('DW02Y1__', ('DW23', 1)), ('DW02Y2__', ('DW24', 1)), ('DW03V1__', ('DW31', 1)), ('DW03V2__', ('DW32', 1)),
    ('DW03Y1__', ('DW33', 1)), ('DW03Y2__', ('DW34', 1)), ('DW04U1__', ('DW43', 1)), ('DW04U2__', ('DW44', 1)),
    ('DW04Y1__', ('DW41', 1)), ('DW04Y2__', ('DW42', 1)), ('DW05V1__', ('DW53', 1)), ('DW05V2__', ('DW54', 1)),
    ('DW05X1__', ('DW51', 1)), ('DW05X2__', ('DW52', 1)), ('DW06U1__', ('DW61', 1)), ('DW06U2__', ('DW62', 1)),
    ('DW06X1__', ('DW63', 1)), ('DW06X2__', ('DW64', 1)), ('FI01X1__', ('FI01', 1)), ('FI01Y1__', ('FI01', 2)),
    ('FI02X1__', ('FI02', 1)), ('FI02Y1__', ('FI02', 2)), ('FI03U1__', ('FI03', 3)), ('FI03X1__', ('FI03', 1)),
    ('FI03Y1__', ('FI03', 2)), ('FI04U1__', ('FI04', 3)), ('FI04X1__', ('FI04', 1)), ('FI04Y1__', ('FI04', 2)),
    ('FI05X1__', ('FI05', 1)), ('FI05Y1__', ('FI05', 2)), ('FI06V1__', ('FI06', 3)), ('FI06X1__', ('FI06', 1)),
    ('FI06Y1__', ('FI06', 2)), ('FI07X1__', ('FI07', 1)), ('FI07Y1__', ('FI07', 2)), ('FI08X1__', ('FI08', 1)),
    ('FI08Y1__', ('FI08', 2)), ('FI15U1__', ('FI15', 3)), ('FI15X1__', ('FI15', 1)), ('FI15Y1__', ('FI15', 2)),
    ('FI55U1__', ('FI55', 1)), ('FI55V1__', ('FI55', 2)), ('GM01U1__', ('MG21', 1)), ('GM01V1__', ('MG22', 1)),
    ('GM01X1__', ('MG11', 1)), ('GM01Y1__', ('MG12', 1)), ('GM02U1__', ('MG21', 2)), ('GM02V1__', ('MG22', 2)),
    ('GM02X1__', ('MG11', 2)), ('GM02Y1__', ('MG12', 2)), ('GM03U1__', ('MG21', 3)), ('GM03V1__', ('MG22', 3)),
    ('GM03X1__', ('MG11', 3)), ('GM03Y1__', ('MG12', 3)), ('GM04U1__', ('MG21', 4)), ('GM04V1__', ('MG22', 4)),
    ('GM04X1__', ('MG11', 4)), ('GM04Y1__', ('MG12', 4)), ('GM05U1__', ('MG21', 5)), ('GM05V1__', ('MG22', 5)),
    ('GM05X1__', ('MG11', 5)), ('GM05Y1__', ('MG12', 5)), ('GM06U1__', ('MG21', 6)), ('GM06V1__', ('MG22', 6)),
    ('GM06X1__', ('MG11', 6)), ('GM06Y1__', ('MG12', 6)), ('GM07U1__', ('MG21', 7)), ('GM07V1__', ('MG22', 7)),
    ('GM07X1__', ('MG11', 7)), ('GM07Y1__', ('MG12', 7)), ('GM08U1__', ('MG21', 8)), ('GM08V1__', ('MG22', 8)),
    ('GM08X1__', ('MG11', 8)), ('GM08Y1__', ('MG12', 8)), ('GM09U1__', ('MG21', 9)), ('GM09V1__', ('MG22', 9)),
    ('GM09X1__', ('MG11', 9)), ('GM09Y1__', ('MG12', 9)), ('GM10U1__', ('MG21', 10)), ('GM10V1__', ('MG22', 10)),
    ('GM10X1__', ('MG11', 10)), ('GM10Y1__', ('MG12', 10)), ('GM11U1__', ('MG41', 1)), ('GM11V1__', ('MG42', 1)),
    ('GM11X1__', ('MG31', 1)), ('GM11Y1__', ('MG32', 1)), ('GP02P1__', ('PG43', 2)), ('GP02P1__', ('PG44', 2)),
    ('GP02P2__', ('PG33', 2)), ('GP02P2__', ('PG34', 2)), ('GP02U1__', ('PG41', 2)), ('GP02V1__', ('PG42', 2)),
    ('GP02X1__', ('PG32', 2)), ('GP02Y1__', ('PG31', 2)), ('GP03P1__', ('PG43', 3)), ('GP03P1__', ('PG44', 3)),
    ('GP03P2__', ('PG33', 3)), ('GP03P2__', ('PG34', 3)), ('GP03U1__', ('PG41', 3)), ('GP03V1__', ('PG42', 3)),
    ('GP03X1__', ('PG32', 3)), ('GP03Y1__', ('PG31', 3)), ('HG01Y1__', ('HG11', 1)), ('HG01Y1__', ('HG12', 1)),
    ('HG01Y1__', ('HG13', 1)), ('HG01Y1__', ('HG14', 1)), ('HG01Y1__', ('HG15', 1)), ('HG02Y1__', ('HGJ1', 1)),
    ('HG02Y2__', ('HGS1', 1)), ('HI05X1_d', ('H5D1', 1)), ('HI05X1_u', ('H5D2', 1)), ('HL04X1_m', ('H4L1', 1)),
    ('HL04X1_m', ('H4L2', 1)), ('HL04X1_m', ('H4L3', 1)), ('HL04X1_m', ('H4L4', 1)), ('HL05X1_m', ('H5L1', 1)),
    ('HL05X1_m', ('H5L2', 1)), ('HL05X1_m', ('H5L3', 1)), ('HL05X1_m', ('H5L4', 1)), ('HM04X1_d', ('H4U1', 1)),
    ('HM04X1_u', ('H4U2', 1)), ('HM04Y1_d', ('H4G1', 1)), ('HM04Y1_d', ('H4G2', 1)), ('HM04Y1_u', ('H4G3', 1)),
    ('HM04Y1_u', ('H4G4', 1)), ('HM05X1_d', ('H5U1', 1)), ('HM05X1_u', ('H5U2', 1)), ('HM05Y1_d', ('H5G1', 1)),
    ('HM05Y1_d', ('H5G2', 1)), ('HM05Y1_u', ('H5G3', 1)), ('HM05Y1_u', ('H5G4', 1)), ('HO03Y1_m', ('H3H1', 1)),
    ('HO03Y1_m', ('H3H2', 1)), ('HO03Y1_m', ('H3H3', 1)), ('HO03Y1_m', ('H3H4', 1)), ('HO03Y1_m', ('H3H5', 1)),
    ('HO04Y1_m', ('H4A1', 1)), ('HO04Y1_m', ('H4A2', 1)), ('HO04Y1_m', ('H4A3', 1)), ('HO04Y1_m', ('H4A4', 1)),
    ('HO04Y1_m', ('H4A5', 1)), ('HO04Y2_m', ('H4B1', 1)), ('HO04Y2_m', ('H4B2', 1)), ('HO04Y2_m', ('H4B3', 1)),
    ('HO04Y2_m', ('H4B4', 1)), ('HO04Y2_m', ('H4B5', 1)), ('MA01X1__', ('MWA1', 1)), ('MA01X2__', ('MWA2', 1)),
    ('MA01X3__', ('MWA1', 2)), ('MA01X4__', ('MWA2', 2)), ('MA01Y1__', ('MWA3', 1)), ('MA01Y2__', ('MWA4', 1)),
    ('MA01Y3__', ('MWA3', 2)), ('MA01Y4__', ('MWA4', 2)), ('MA02X1__', ('MWA1', 3)), ('MA02X2__', ('MWA2', 3)),
    ('MA02X3__', ('MWA1', 4)), ('MA02X4__', ('MWA2', 4)), ('MA02Y1__', ('MWA3', 3)), ('MA02Y2__', ('MWA4', 3)),
    ('MA02Y3__', ('MWA3', 4)), ('MA02Y4__', ('MWA4', 4)), ('MB01V1db', ('MBV2', 1)), ('MB01V1dc', ('MBV4', 1)),
    ('MB01V1ub', ('MBV1', 1)), ('MB01V1uc', ('MBV3', 1)), ('MB01X1db', ('MBY2', 1)), ('MB01X1dc', ('MBY4', 1)),
    ('MB01X1ub', ('MBY1', 1)), ('MB01X1uc', ('MBY3', 1)), ('MB01Y1dl', ('MBZ4', 1)), ('MB01Y1dr', ('MBZ2', 1)),
    ('MB01Y1ul', ('MBZ3', 1)), ('MB01Y1ur', ('MBZ1', 1)), ('MB02V2db', ('MBV2', 2)), ('MB02V2dc', ('MBV4', 2)),
    ('MB02V2ub', ('MBV1', 2)), ('MB02V2uc', ('MBV3', 2)), ('MB02X2db', ('MBY2', 2)), ('MB02X2dc', ('MBY4', 2)),
    ('MB02X2ub', ('MBY1', 2)), ('MB02X2uc', ('MBY3', 2)), ('MB02Y2dl', ('MBZ4', 2)), ('MB02Y2dr', ('MBZ2', 2)),
    ('MB02Y2ul', ('MBZ3', 2)), ('MB02Y2ur', ('MBZ1', 2)), ('MP01MU__', ('PMU4', 1)), ('MP01MU__', ('PMU5', 1)),
    ('MP01MV__', ('PMV4', 1)), ('MP01MV__', ('PMV5', 1)), ('MP01MX__', ('PMX4', 1)), ('MP01MX__', ('PMX5', 1)),
    ('MP01MY__', ('PMY4', 1)), ('MP01MY__', ('PMY5', 1)), ('MP01U1__', ('PMU1', 1)), ('MP01U1__', ('PMU2', 1)),
    ('MP01U1__', ('PMU3', 1)), ('MP01V1__', ('PMV1', 1)), ('MP01V1__', ('PMV2', 1)), ('MP01V1__', ('PMV3', 1)),
    ('MP01X1__', ('PMX1', 1)), ('MP01X1__', ('PMX2', 1)), ('MP01X1__', ('PMX3', 1)), ('MP01Y1__', ('PMY1', 1)),
    ('MP01Y1__', ('PMY2', 1)), ('MP01Y1__', ('PMY3', 1)), ('MP02MU__', ('PMU4', 2)), ('MP02MU__', ('PMU5', 2)),
    ('MP02MV__', ('PMV4', 2)), ('MP02MV__', ('PMV5', 2)), ('MP02MX__', ('PMX4', 2)), ('MP02MX__', ('PMX5', 2)),
    ('MP02MY__', ('PMY4', 2)), ('MP02MY__', ('PMY5', 2)), ('MP02U1__', ('PMU1', 2)), ('MP02U1__', ('PMU2', 2)),
    ('MP02U1__', ('PMU3', 2)), ('MP02V1__', ('PMV1', 2)), ('MP02V1__', ('PMV2', 2)), ('MP02V1__', ('PMV3', 2)),
    ('MP02X1__', ('PMX1', 2)), ('MP02X1__', ('PMX2', 2)), ('MP02X1__', ('PMX3', 2)), ('MP02Y1__', ('PMY1', 2)),
    ('MP02Y1__', ('PMY2', 2)), ('MP02Y1__', ('PMY3', 2)), ('MP03MU__', ('PMU4', 3)), ('MP03MU__', ('PMU5', 3)),
    ('MP03MV__', ('PMV4', 3)), ('MP03MV__', ('PMV5', 3)), ('MP03MX__', ('PMX4', 3)), ('MP03MX__', ('PMX5', 3)),
    ('MP03MY__', ('PMY4', 3)), ('MP03MY__', ('PMY5', 3)), ('MP03U1__', ('PMU1', 3)), ('MP03U1__', ('PMU2', 3)),
    ('MP03U1__', ('PMU3', 3)), ('MP03V1__', ('PMV1', 3)), ('MP03V1__', ('PMV2', 3)), ('MP03V1__', ('PMV3', 3)),
    ('MP03X1__', ('PMX1', 3)), ('MP03X1__', ('PMX2', 3)), ('MP03X1__', ('PMX3', 3)), ('MP03Y1__', ('PMY1', 3)),
    ('MP03Y1__', ('PMY2', 3)), ('MP03Y1__', ('PMY3', 3)), ('PA01U1__', ('PA01', 2)), ('PA01V1__', ('PA01', 3)),
    ('PA01X1__', ('PA01', 1)), ('PA02U1__', ('PA02', 2)), ('PA02V1__', ('PA02', 3)), ('PA02X1__', ('PA02', 1)),
    ('PA03U1__', ('PA03', 2)), ('PA03V1__', ('PA03', 3)), ('PA03X1__', ('PA03', 1)), ('PA04U1__', ('PA04', 2)),
    ('PA04V1__', ('PA04', 3)), ('PA04X1__', ('PA04', 1)), ('PA05U1__', ('PA05', 2)), ('PA05V1__', ('PA05', 3)),
    ('PA05X1__', ('PA05', 1)), ('PA06U1__', ('PA06', 2)), ('PA06V1__', ('PA06', 3)), ('PA06X1__', ('PA06', 1)),
    ('PA11U1__', ('PA11', 2)), ('PA11V1__', ('PA11', 3)), ('PA11X1__', ('PA11', 1)), ('PB01U1__', ('PB01', 2)),
    ('PB01X1__', ('PB01', 1)), ('PB02V1__', ('PB02', 1)), ('PB03U1__', ('PB03', 2)), ('PB03X1__', ('PB03', 1)),
    ('PB04V1__', ('PB04', 1)), ('PB05U1__', ('PB05', 2)), ('PB05X1__', ('PB05', 1)), ('PB06V1__', ('PB06', 1)),
    ('PS01U1__', ('PS01', 3)), ('PS01V1__', ('PS01', 4)), ('PS01X1__', ('PS01', 2)), ('PS01Y1__', ('PS01', 1)),
    ('SI01U1__', ('SIU1', 1)), ('SI01V1__', ('SIU2', 1)), ('SI01X1__', ('SID2', 1)), ('SI01Y1__', ('SID1', 1)),
    ('SI02U1__', ('SIU1', 2)), ('SI02V1__', ('SIU2', 2)), ('SI02X1__', ('SID2', 2)), ('SI02Y1__', ('SID1', 2)),
    ('SI03U1__', ('SIU1', 3)), ('SI03V1__', ('SIU2', 3)), ('SI03X1__', ('SID2', 3)), ('SI03Y1__', ('SID1', 3)),
    ('ST03U1da', ('STU6', 1)), ('ST03U1db', ('STU2', 1)), ('ST03U1dc', ('STU4', 1)), ('ST03U1ua', ('STU5', 1)),
    ('ST03U1ub', ('STU1', 1)), ('ST03U1uc', ('STU3', 1)), ('ST03V1da', ('STV6', 1)), ('ST03V1db', ('STV2', 1)),
    ('ST03V1dc', ('STV4', 1)), ('ST03V1ua', ('STV5', 1)), ('ST03V1ub', ('STV1', 1)), ('ST03V1uc', ('STV3', 1)),
    ('ST03X1da', ('STY6', 1)), ('ST03X1db', ('STY2', 1)), ('ST03X1dc', ('STY4', 1)), ('ST03X1ua', ('STY5', 1)),
    ('ST03X1ub', ('STY1', 1)), ('ST03X1uc', ('STY3', 1)), ('ST03X2da', ('STY6', 2)), ('ST03X2db', ('STY2', 2)),
    ('ST03X2dc', ('STY4', 2)), ('ST03X2ua', ('STY5', 2)), ('ST03X2ub', ('STY1', 2)), ('ST03X2uc', ('STY3', 2)),
    ('ST03Y1da', ('STZ6', 1)), ('ST03Y1db', ('STZ2', 1)), ('ST03Y1dc', ('STZ4', 1)), ('ST03Y1ua', ('STZ5', 1)),
    ('ST03Y1ub', ('STZ1', 1)), ('ST03Y1uc', ('STZ3', 1)), ('ST03Y2da', ('STZ6', 2)), ('ST03Y2db', ('STZ2', 2)),
    ('ST03Y2dc', ('STZ4', 2)), ('ST03Y2ua', ('STZ5', 2)), ('ST03Y2ub', ('STZ1', 2)), ('ST03Y2uc', ('STZ3', 2)),
    ('VI01P1__', ('VI11', 1)), ('VI02X1__', ('VI21', 1)), ('VO01X1__', ('VO11', 1)),
]
TBNAMES = list(zip(*DB_CFG))[0]


def execute(command: str) -> int:
    """
    Executes shell @command and returns a return code
    :param command - string with a shell command
    :return -  integer of a return code
    """
    command_expanded = expandvars(command)
    logger.info(f'Executing: {command_expanded}')
    proc = Popen(expandvars(command_expanded), shell=True, bufsize=0)
    proc.communicate()
    proc.wait()
    return proc.returncode


def arguments(filename: str) -> List[Tuple[str, str, int]]:
    """
    Generate list of executable arguments for a given histograam file
    :param filename - path to a histogram file
    :return list of a tuples containing (tbname, detname, unit)
    """
    tbname_base = basename(filename).split('_')[0]
    res = []
    for tbname, (detname, unit) in DB_CFG:
        if tbname.startswith(tbname_base):
            res.append((tbname, detname, unit))
    return res


def prepare_histogram_processor(tbname, collective_root, cuts_norm, nocuts_norm):
    histogram_builder_tlp_file = join(abspath('.'), 'tbname_hists.cc')
    histogram_builder_tlp = Template(filename=histogram_builder_tlp_file)
    args = dict(
        tbname=tbname,
        collective_root=collective_root,
        cuts_norm=cuts_norm,
        nocuts_norm=nocuts_norm,
    )
    return f'make_histograms_{tbname}', histogram_builder_tlp.render(**args)

if __name__ == '__main__':
    aparser = ArgumentParser(description='Assembles the SQLite database of given histogram files')
    aparser.add_argument('--dbpath', type=str, required=True, help='Output database path')
    aparser.add_argument('--year', type=str, required=True, help='Year to which register histograms')
    aparser.add_argument('--detectors', type=str, help='Used to initialize a new year')

    args = aparser.parse_args()
    dbpath = args.dbpath
    dbyear = args.year
    detectors_dat = args.detectors

    logger.info(f'Creating database at {dbpath}')
    if not exists(dbpath):
        execute(f'python3 ${{TGEANT}}/bin/dbcreate.py {dbpath}')

    if detectors_dat is not None:
        execute(f'${{TGEANT}}/bin/efficienciesDB {dbpath} {detectors_dat} {dbyear}')

    makedirs('RT', exist_ok=True)
    makedirs('err', exist_ok=True)
    makedirs('png', exist_ok=True)
    makedirs('maps', exist_ok=True)

    for fname in stdin.readlines():
        merged_ue11 = abspath(normpath(fname.strip()))
        for tbname, detname, unit in arguments(merged_ue11):
            hname, hcontent = prepare_histogram_processor(tbname, merged_ue11, 1, 1)
            hist_generator = hname + '.cc'
            with open(hist_generator, 'w') as hcc:
                hcc.write(hcontent)

            execute(f'root -x -q -l -b {hist_generator}')
            execute(f'rm {hist_generator}')

            hist_path = abspath(normpath(join('.', 'maps', f'{tbname}.root')))
            smoothed_hist_path = abspath(normpath(join('.', 'maps', f'smoothed_{tbname}.root')))
            execute(f'python ${{TGEANT}}/bin/efficConverter.py -s {hist_path} {tbname} {smoothed_hist_path}')
            execute(f'${{TGEANT}}/bin/efficienciesDB {dbpath} {tbname} {detname} {unit} {dbyear} {smoothed_hist_path}')
